/* Core */
import React from 'react';
import ReactDOM from 'react-dom';

/*Inner Components*/
import {ModalApp} from "./components/AppModal"

/* Theme */


ReactDOM.render(
  < ModalApp/>,
  document.getElementById('root'),
);
