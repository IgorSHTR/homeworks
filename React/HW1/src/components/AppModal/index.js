import React, {useState} from "react";
import modalscont from "../modalsContent";
import {Modal}from "../Modals";
import "../Modals/style.css";
import{Buttons} from "../Buttons"


export const ModalApp = () => {
 const [isModalOpen, setModalState] = useState (false);
 const [pressedButton, setPressedButton] = useState (2);


return(
 
 <main className = "container" style ={{height: "100%"}}>
< Modal isModalOpen={isModalOpen} setModalState={setModalState} pressedButton ={pressedButton} setPressedButton = {setPressedButton}/> 
< Buttons isModalOpen={isModalOpen} setModalState={setModalState} pressedButton ={pressedButton} setPressedButton = {setPressedButton}/>
</main>
       
)   
}