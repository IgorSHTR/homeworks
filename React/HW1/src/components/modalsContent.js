export default[
    {
        "id":"123",
        "header":"Do you want to delete this file?",
        "content":"Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?",
        "styles" :{
            color: "wight"
        }
    },
    {
        "id":"456",
        "header":" Do you want to delete this FOLDER?",
        "content":"Once you delete this FOLDER, it won’t be possible to undo this action.Are you sure you want to delete it?"
    }
]