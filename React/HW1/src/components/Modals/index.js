import React,{useState, useEffect} from "react";
import modalcont from "../modalsContent";
import {ConfirmBtns} from "../ButtonsModals";
// import dialogPolyfill from "dialog-polyfill";
import "./style.css"


export const Modal = (props) =>{
    const {isModalOpen, setModalState} = props;
    const {pressedButton, setPressedButton} = props;
    const closeModal =()=>setModalState(false);
    const content = (pressedButton == 0) ? (modalcont[0]) : (modalcont[1]);
    // useEffect( ()=> {
    //     const dialog = document.querySelector("dialog");
    //     dialogPolyfill.registerDialog(dialog)
    //     console.log(dialog)
    //   }, []
      
    //   )
  useEffect (() => {
    if(isModalOpen){
       const  holeWind = document.getElementById("root");
       holeWind.addEventListener("click", (e)=>{
        if (!e.target.closest(".dialog")) {closeModal()}
        console.log(e.target)
       })
        console.log("Осуществиласть перестройка ДОМА")
    }

  },[isModalOpen]) 

    return(
        <dialog className="dialog" open= {isModalOpen} >
            <div className="modal-cont">
                <header className="modal-cont_header"> 
                    <span className="closeCross"onClick={closeModal}
                        >X</span>
                    
                    {content.header}
                </header> 
            <main className="modal-cont_content" >
                {content.content} 
            </main>
                    <ConfirmBtns pressedButton ={pressedButton} isModalOpen={isModalOpen} setModalState={setModalState}/>
       </div>
</dialog>
      
    )

} 

