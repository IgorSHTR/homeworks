import React from "react";

export const Buttons = (props)=>{
    const {isModalOpen, setModalState} = props;
    const {pressedButton, setPressedButton} = props;
    const openModal =(e) => {
        setModalState(true);
        e.target.classList[0].includes("btnFirst") ? setPressedButton(0) : setPressedButton(1)
        console.log(pressedButton)
    }
    return(
   
        <div >
          <button className = "btnFirst" onClick = {openModal} style={{ backgroundColor: "red"}}> 
                Open first modall
            </button>
            <button className = "btnSecond" onClick = {openModal}> 
                Open second modall
            </button>
        </div>
        )
}