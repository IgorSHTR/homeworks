import React from  "react";
import "./styles.scss"

 export const ConfirmBtns = (props) => {
     const {pressedButton}=props;
    const {isModalOpen, setModalState}=props;
    const closeModal =()=>setModalState(false);
    console.log(isModalOpen)

    return (
        <div className = {(pressedButton==0) ? ("contConfBtns__themeRed") : ("contConfBtns__themePerple")}>
          <button  > 
                OK
            </button>
            <button 
            //  onClick={setModalState(closeModal)}
             > 
                Cancel
            </button>
        </div>
    )
 }