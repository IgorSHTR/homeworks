import React,{useState} from "react";
import pList from "../ProdList";
import "./style.scss"

export const ProdModal = (props)=>{
    const {isModalOpen, setModalState} = props;
    const {prodList, setProdList}=props;
    const {itemIndex}=props;

    const   closeModal =(e) => {
        setModalState(false);
      
    }
    const addItem =()=>{
        localStorage.setItem(`buscet${itemIndex}`,JSON.stringify(prodList[itemIndex]));
        setModalState(false)
    }
   const chosenItem =(prodList) && prodList.map((item, index) => 
        {
        if (index ==itemIndex) {
                    
             return (
                <main className="modal-cont_content" >
                    <img  src ={item.fotoLinck}></img> 
                    <div className = "deskr">
                       
                        <div className="deskr__text">
                            <h2>Подтвердите добавления товара в корзину</h2>
                            <h3>{item.name}</h3>
                            <p>{item.description}</p>
                        </div>
                        <div className="orderblock">
                            <div className="priceModal">
                                <div>$</div>
                                <div>{item.price}</div>
                            </div>
                            <div className="btnBay" onClick={addItem}>ADD TO CARD</div> 
                        </div>
                       
                    </div>
                    
                        
                </main>)
            }     
        }

   )
 
     
 
    return(
        <dialog className="dialog" open= {isModalOpen} >
            <div className="modal-cont">
                <header className="modal-cont_header"> 
                    <span className="closeCross"onClick={closeModal}
                        >X</span>
                                  
                </header> 
               {chosenItem}
                   
            </div>
    </dialog>
        )
}