/* Core */
import React from 'react';
import ReactDOM from 'react-dom';

/*Inner Components*/
import {ShopApp} from "./components/App"

/* Theme */


ReactDOM.render(
  < ShopApp/>,
  document.getElementById('root'),
);
