// переключение меню хедера

let hedMenu = $(".menuItems li");
$(hedMenu).on("click", function(){
    $(this).siblings().removeClass("hadMenAct");
    $(this).addClass("hadMenAct");
})

// переключение меню Our Services
let servMenu = $(".servMen li");
$(servMenu).on("click", function(){
let minBut = $(this).data("target");
console.log(minBut);
$(this).siblings().removeClass("cliked");
$(this).addClass("cliked");

let servText =$(".servtex");
console.log(servText);
for (let i = 0; i< servText.length; i++ ){
servText[i].classList.remove("act");
if ($(servText[i]).hasClass(minBut)){
servText[i].classList.add("act");
}
}
})
// переключатели Our Amazing Work (all показывает
//  не все картинки подгружает еще 12 из след ктегории)

$(document).ready(function () {
    $(".portfolio-item .item").hide().slice(0, 12).show();
    $(".amm-wk li").click(function () {
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        const selector = $(this).attr("data-filter");
        console.log(this);
        if (selector){
        $(".portfolio-item .item").fadeOut(400);
        $(`.portfolio-item ${selector}.item`).fadeIn(400);
        let itrms = $(`.portfolio-item ${selector}.item`)
        console.log(itrms.length);
        if ($(`.portfolio-item ${selector}.item`).length <= 12) {
           
            $(".button_load_more").fadeOut(400)
        }
        else {
            
            $(".button_load_more").fadeIn(400)
        }
    }
    else{
        $(".portfolio-item .item").hide().slice(0, 12).show();
        $(".button_load_more").fadeIn(400)
    }
    });
    $(".button_load_more").click(function () {
      
           const hideSlise = $(".portfolio-item .item:hidden").slice(0, 12);
           $(".portfolio-item .item").hide();
           $(hideSlise).fadeIn(1000);
           $(".button_load_more").fadeOut(400)
        })
  })



// ховер накартинки Our Amazing Work

$(".item").hover(function (e) {
    
    const type = $(this);
    $(this).prepend(`<div class="hover-wrapper">
    <div class="hover-boxes">
        <div class="link-box">
                <i class="fa fa-link" aria-hidden="true"></i>
        </div>
        <div class="search-box">
                <i class="fas fa-search"></i>
        </div>
    </div>
    <p class="first-hover-text">creative design</p>
    <p class="second-hover-text">Web Design</p>
    </div>`);
    }, function () {
        // $( this ).find(“img”).show();
        $( this ).find(".hover-wrapper").remove();
    }
 );

// слик слайдер


$(".emploee-data").eq(0).fadeIn(1000).siblings().hide();
 $(".emploee").on("click", function(e){
    $(this).addClass("slick-center").siblings().removeClass("slick-center");
    const actIndex = $(this).index();
    console.log(actIndex );
    mainInf(actIndex);
 })
$(".move-arrows").on("click", function(){
    if ($(this).hasClass("prew-arrow")){
        const focusSlide = $(".slick-center").index();
        console.log(focusSlide);
        const prewFocusSlide = focusSlide  == 0 ? 3 : focusSlide-1;
        console.log(prewFocusSlide);
        $(`.emploee:eq(${prewFocusSlide})`).addClass("slick-center").siblings().removeClass("slick-center");
        mainInf (prewFocusSlide);
    }
    if ($(this).hasClass("next-arrow")){
        const focusSlide = $(".slick-center").index();
        const nextFocusSlide = focusSlide  == 3 ? 0 : focusSlide+1;
        console.log(nextFocusSlide);
        $(`.emploee:eq(${nextFocusSlide})`).addClass("slick-center").siblings().removeClass("slick-center");
        mainInf (nextFocusSlide);
    }

 }
)

function mainInf (a){
     
    $(".emploee-data").eq(a)
    .fadeIn(1000)
    .siblings()
    .hide();
       }